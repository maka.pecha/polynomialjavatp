import static org.junit.jupiter.api.Assertions.*;

class PolynomialTest {
    private Polynomial poli_1;
    private Polynomial poli_2;
    private Polynomial poli_3;
    private int[] array1 = {1, 2, -3, 4};
    private int[] array2 = {-4, 3, 7, 9, 5};

    @org.junit.jupiter.api.BeforeAll
    static void setUpClass() {
        System.out.println("---Maka Pecha---");
        System.out.println("---Begin Tests---");
    }

    @org.junit.jupiter.api.AfterAll
    static void tearDownClass() {
        System.out.println("---End Tests---");
    }

    @org.junit.jupiter.api.BeforeEach
    void setUp() {

        poli_1 = new Polynomial(array1);
        poli_2 = new Polynomial(array2);
        poli_3 = new Polynomial();
        System.out.println("---Test---\n");
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
        poli_1 = poli_2 = poli_3 = null;
    }

    @org.junit.jupiter.api.Test
    void testDefaultPolynomial() {
        String esperado1 = "P(x) = 0";
        Polynomial poli_esperado = new Polynomial();


        assertEquals(esperado1, poli_esperado.toString());
        System.out.println("Polinomio esperado: " + esperado1);
        System.out.println("Polinomio obtenido: " + poli_esperado.toString());
    }

    @org.junit.jupiter.api.Test
    void testPolynomial() {
        int[] array = {9, 8, 0, 7, -6, -5, 4, 0};
        String esperado1 = "P(x) = 4X^6 -5X^5 -6X^4 +7X^3 +8X^1 +9";
        Polynomial poli_esperado = new Polynomial(array);


        assertEquals(esperado1, poli_esperado.toString());
        System.out.println("Polinomio esperado: " + esperado1);
        System.out.println("Polinomio obtenido: " + poli_esperado.toString());
    }

    @org.junit.jupiter.api.Test
    void testAdd() {
        int[] array = {-3, 5, 4, 13, 5};
        Polynomial poli_esperado = new Polynomial(array);

        System.out.println("Suma:");
        System.out.println(poli_1.toString() + "\n  +\n" + poli_2.toString() + "\n");

        assertEquals(poli_1.add(poli_2), poli_esperado);
        System.out.println("Polinomio esperado: " + poli_esperado.toString());
        System.out.println("Polinomio obtenido: " + poli_1.add(poli_2).toString());
    }

    @org.junit.jupiter.api.Test
    void testGetCoefficient1() {
        System.out.println("Polinomio de entrada: " + poli_1.toString());
        assertEquals(0, poli_1.getCoefficient(4), "Not Equals");
        assertEquals(1, poli_1.getCoefficient(0), "Not Equals");
        assertEquals(2, poli_1.getCoefficient(1), "Not Equals");
        assertEquals(0, poli_1.getCoefficient(-5), "Not Equals");
    }

    @org.junit.jupiter.api.Test
    void testGetCoefficient2() {
        System.out.println("Polinomio de entrada: " + poli_2.toString());
        assertEquals(-4, poli_2.getCoefficient(0), "Not Equals");
        assertEquals(3, poli_2.getCoefficient(1), "Not Equals");
        assertEquals(7, poli_2.getCoefficient(2), "Not Equals");
        assertEquals(9, poli_2.getCoefficient(3), "Not Equals");
        assertEquals(5, poli_2.getCoefficient(4), "Not Equals");
        assertEquals(0, poli_2.getCoefficient(-4), "Not Equals");
    }


    @org.junit.jupiter.api.Test
    void testSetCoefficient1() {
        int[] array = {2, 100, -5, 0, 0, 8};
        Polynomial poli_esperado = new Polynomial(array);

        System.out.println("Entrada: " + poli_1.toString() + "\n");
        poli_1.setCoefficient(0, 2);
        poli_1.setCoefficient(1, 100);
        poli_1.setCoefficient(2, -5);
        poli_1.setCoefficient(3, 0);
        poli_1.setCoefficient(5, 8);
        System.out.println("Polinomio esperado: " + poli_esperado.toString());
        System.out.println("Polinomio obtenido: " + poli_1.toString() + "\n");

        assertEquals(2, poli_1.getCoefficient(0), "Not Equals");
        assertEquals(100, poli_1.getCoefficient(1), "Not Equals");
        assertEquals(-5, poli_1.getCoefficient(2), "Not Equals");
        assertEquals(0, poli_1.getCoefficient(3), "Not Equals");
        assertEquals(0, poli_1.getCoefficient(4), "Not Equals");
        assertEquals(8, poli_1.getCoefficient(5), "Not Equals");
    }

        //----------------------------------------------------------
    @org.junit.jupiter.api.Test
    void testSetCoefficient2() {
        int[] array2 = {2, -5, 5, 0, 5, -25};
        Polynomial poli_esperado2 = new Polynomial(array2);

        System.out.println("Entrada: " + poli_2.toString() +"\n");
        poli_2.setCoefficient(0,2);
        poli_2.setCoefficient(1,-5);
        poli_2.setCoefficient(2,5);
        poli_2.setCoefficient(3,0);
        poli_2.setCoefficient(5,-25);
        System.out.println("Polinomio esperado: " + poli_esperado2.toString());
        System.out.println("Polinomio de salida: " + poli_2.toString());

        assertEquals(2, poli_2.getCoefficient(0), "Not Equals");
        assertEquals(-5, poli_2.getCoefficient(1), "Not Equals");
        assertEquals(5, poli_2.getCoefficient(2), "Not Equals");
        assertEquals(0, poli_2.getCoefficient(3), "Not Equals");
        assertEquals(5, poli_2.getCoefficient(4), "Not Equals");
        assertEquals(-25, poli_2.getCoefficient(5), "Not Equals");

    }

    @org.junit.jupiter.api.Test
    void testValueOf1() {
        float entrada1 = 15;
        float esperado1 = 12856;

        System.out.println("Polinomio de entrada: " + poli_1.toString());

        assertEquals(esperado1, poli_1.valueOf(entrada1));

        System.out.println("Valor de entrada: " + entrada1);
        System.out.println("Resultado esperado: " + esperado1);
    }

    @org.junit.jupiter.api.Test
    void testValueOf2() {
        float entrada2 = -3;
        float esperado2 = 212;

        System.out.println("Polinomio de entrada: " + poli_2.toString());

        assertEquals(esperado2, poli_2.valueOf(entrada2), "Not Equals");

        System.out.println("Valor de entrada: " + entrada2);
        System.out.println("Resultado esperado: " + esperado2);
    }

    @org.junit.jupiter.api.Test
    void testEquals1() {
        int num = 5;
        Polynomial muestra = poli_1;
        System.out.println("Entradas: \nMuestra: " + muestra.toString() + "\nPolinomio 1: " + poli_1.toString());
        System.out.println("Polinomio 2: " + poli_2.toString() + "\nPolinomio 3: " + poli_3.toString() + "\nNúmero: " + num + "\n");

        assertEquals(muestra, poli_1, "False");
        assertNotEquals(muestra, poli_2, "True");
        assertNotEquals(muestra, poli_3, "True");
        assertNotEquals(muestra, num, "True");

        this.resultadosEquals(muestra, num);
    }

    @org.junit.jupiter.api.Test
    void testEquals2() {
        int num = 5;
        Polynomial muestra = poli_2;
        System.out.println("Entradas: \nMuestra: " + muestra.toString() + "\nPolinomio 1: " + poli_1.toString());
        System.out.println("Polinomio 2: " + poli_2.toString() + "\nPolinomio 3: " + poli_3.toString() + "\nNúmero: " + num + "\n");

        assertNotEquals(muestra, poli_1, "True");
        assertEquals(muestra, poli_2, "False");
        assertNotEquals(muestra, poli_3, "True");
        assertNotEquals(muestra, num, "True");

        this.resultadosEquals(muestra, num);
    }

    @org.junit.jupiter.api.Test
    void testToString1() {
        String esperado1 = "P(x) = 4X^3 -3X^2 +2X^1 +1";

        System.out.println("Entrada: " + esperado1);
        System.out.println("Polinomio Esperado: " + esperado1);
        System.out.println("Polinomio Obtenido: " + poli_1.toString());

        assertEquals(poli_1.toString(), esperado1, "False");
    }

    @org.junit.jupiter.api.Test
    void testToString2() {
        String esperado2 = "P(x) = 5X^4 +9X^3 +7X^2 +3X^1 -4";

        System.out.println("Entrada: " + esperado2);
        System.out.println("Polinomio Esperado: " + esperado2);
        System.out.println("Polinomio Obtenido: " + poli_2.toString());

        assertEquals(poli_2.toString(), esperado2, "False");
    }

    void resultadosEquals(Polynomial muestra, int num) {
        System.out.println("Resultados: ");
        if (muestra.equals(poli_1)){
            System.out.println("Muestra y Polinomio 1 son iguales");
        } else {
            System.out.println("Muestra y Polinomio 1 no son iguales");
        } if (muestra.equals(poli_2)){
            System.out.println("Muestra y Polinomio 2 son iguales");
        } else {
            System.out.println("Muestra y Polinomio 2 no son iguales");
        } if (muestra.equals(poli_3)){
            System.out.println("Muestra y Polinomio 3 son iguales");
        } else {
            System.out.println("Muestra y Polinomio 3 no son iguales");
        } if (muestra.equals(num)){
            System.out.println("Muestra y Número son iguales");
        } else {
            System.out.println("Muestra y Número no son iguales");
        }
    }
}