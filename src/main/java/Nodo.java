class Nodo {
    private Nodo nextNodo = null;
    private int exponente;
    private int coeficiente;

    Nodo(int coef, int exp, Nodo nextNodo) {
        this.nextNodo = nextNodo;
        this.coeficiente = coef;
        this.exponente = exp;
    }

    int getExponente() {
        return exponente;
    }

    int getCoeficiente() {
        return coeficiente;
    }

    void setCoeficiente(int coef) {
        this.coeficiente = coef;
    }

    Nodo getNextNodo() {
        return nextNodo;
    }

    void setNextNodo(Nodo nextNodo) {
        this.nextNodo = nextNodo;
    }
}