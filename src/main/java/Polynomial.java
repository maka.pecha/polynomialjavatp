public class Polynomial {
    private Nodo raiz;
    private int size;

    //Polynomial(): este constructor debe crear un objeto Polynomial igual al polinomio
    //0(cero) (grado n = 0).
    Polynomial() { //constructor por defecto del polinomio
        raiz = new Nodo(0, 0, null);
        size = 1;
    }

    //Polynomial(int coef[]): este constructor debe crear un objeto Polynomial cuyo grado
    //sea igual al size del arreglo coef, y cuyos coeficientes sean tomados uno a uno
    //desde el mismo arreglo coef que entra como parámetro, en orden inverso, pero
    //considerando que si algún casillero coef[k] es cero, entonces el término
    //correspondiente en el polinomio no existe y no debe agregarse .
    Polynomial(int[] coef) {
        if ( coef[0] == 0 && coef.length == 1 ) {
            raiz = new Nodo(0, 0, null);
            size = coef.length;
        } else {
            Nodo nuevoNodo = null;
            size = 0;
            for (int i = 0; i < coef.length; i++) {
                if (coef[i] != 0) {
                    nuevoNodo = new Nodo(coef[i], i, nuevoNodo);
                    size++;
                }
            }
            raiz = nuevoNodo;
        }
    }

    //Polynomial add(Polynomial pol): retorna un Polynomial igual a la suma entre this y pol .
    Polynomial add(Polynomial poli) {
        int gradoPoli = poli.getOrder();
        int gradoThis = this.getOrder();
        int[] coefficients;

        Polynomial mayorGrado = (gradoThis >= gradoPoli) ? this : poli;
        Polynomial menorGrado = (gradoThis < gradoPoli) ? this : poli;

        coefficients = new int[mayorGrado.raiz.getExponente() + 1]; //obtener tamaño del array de coef del nuevo poli

        Nodo actualNodoThis = mayorGrado.raiz;
        Nodo actualNodoPoli = menorGrado.raiz;

        for (int coefficient : coefficients) {
            if (actualNodoPoli != null) {
                if (actualNodoThis != null && actualNodoThis.getExponente() == actualNodoPoli.getExponente()) {
                    int exp = actualNodoPoli.getExponente();
                    coefficients[exp] = actualNodoThis.getCoeficiente() + actualNodoPoli.getCoeficiente();
                    actualNodoThis = actualNodoThis.getNextNodo();
                    actualNodoPoli = actualNodoPoli.getNextNodo();
                } else if (actualNodoThis != null &&  actualNodoThis.getExponente() > actualNodoPoli.getExponente()) {
                        int exp = actualNodoThis.getExponente();
                        coefficients[exp] = actualNodoThis.getCoeficiente();
                        actualNodoThis = actualNodoThis.getNextNodo();
                } else {
                        int exp = actualNodoPoli.getExponente();
                        coefficients[exp] = actualNodoPoli.getCoeficiente();
                        actualNodoPoli = actualNodoPoli.getNextNodo();
                }
            }
        }
        return new Polynomial(coefficients);
    }

    //int getCoefficient(int x): Devuelve el valor del coeficiente del grado x
    int getCoefficient(int x) {
        Nodo nodo = this.raiz;
        int coef = 0;
        for (int i = 1; i <= this.size; i++) {
            if (nodo.getExponente() == x) {
                coef = nodo.getCoeficiente();
                break;
            }
            nodo = nodo.getNextNodo();
        }
        return coef;
    }

    //void setCoefficient(int x, int coef): establece el valor del coeficiente de grado x al valor coef
    void setCoefficient(int x, int coef) {
        if(x < 0) return;

        Nodo nuevoNodo = this.raiz;
        Nodo actualNodo = nuevoNodo;

        for (int i = 1; i <= this.size; i++) {
            if (actualNodo.getExponente() == x) {
                if (coef != 0) actualNodo.setCoeficiente(coef);
                else removeNodo(nuevoNodo, actualNodo);
                break;
            }
            if (this.raiz.getExponente() < x ||
                (nuevoNodo.getExponente() > x && actualNodo.getExponente() < x) ||
                actualNodo.getNextNodo() == null) {
                createNodo(nuevoNodo, actualNodo, x, coef);
                break;
            }
            nuevoNodo = actualNodo;
            actualNodo = actualNodo.getNextNodo();
        }
    }

    //float valueOf(float x): calcula y retorna el valor del polinomio en el punto x.
    float valueOf(float x) {
        float acum = 0;
        if (x == 0) return getCoefficient(0);
        Nodo actualNodo = raiz; //el nodo en la raiz

        for (int i = 1; i <= this.size; i++) { //recorro el size para reemplazar x por el valor y obtener el resultado
            int exp = actualNodo.getExponente();
            int coef = actualNodo.getCoeficiente();

            acum += Math.pow(x, exp) * coef;
            actualNodo = actualNodo.getNextNodo(); //paso al siguiente nodo
        }

        return acum;
    }

    //boolean equals(Object x): retorna true si this es igual a x , y false en caso contrario.
    public boolean equals(Object x) {
        if (x == null || this.getClass() != x.getClass()) {
            return false;
        }

        Polynomial poli = (Polynomial) x; //casteo el x a tipo Polynomial
        if (poli.size != this.size) {
            return false;
        }

        if (this == x) {
            return true;
        }

        Nodo actualNodoThis = this.raiz;
        Nodo actualNodoPoli = poli.raiz;

        for (int i = 1; i <= poli.size; i++) {
            if(!(actualNodoPoli.getCoeficiente() == actualNodoThis.getCoeficiente())) return false;
            actualNodoThis = actualNodoThis.getNextNodo();  // si son iguales avanzo al siguiente nodo en ambos
            actualNodoPoli = actualNodoPoli.getNextNodo();
        }
        return true;
    }

    //String toString(): retorna la representación del Polynomial en forma de String .
    @Override
    public String toString() {
        StringBuilder pol = new StringBuilder("P(x) = ");
        Nodo nodo = raiz;
        for (int i = 0; i < size; i++) {
            if(i == 0 || nodo.getCoeficiente() <= 0) {
                if (nodo.getExponente() > 0) {
                    pol.append(nodo.getCoeficiente() + "X^" + nodo.getExponente() + " ");
                } else {
                    pol.append(nodo.getCoeficiente());
                }
            } else {
                if (nodo.getExponente() > 0) {
                    pol.append("+" + nodo.getCoeficiente() + "X^" + nodo.getExponente() + " ");
                } else {
                    pol.append("+" + nodo.getCoeficiente());
                }
            }
            nodo = nodo.getNextNodo();
        }
        return pol.toString();
    }

    private int getOrder() {
        return raiz.getExponente();
    }

    private void createNodo(Nodo nuevoNodo, Nodo actualNodo, int exp, int coef) {
        if (nuevoNodo == actualNodo) this.raiz = new Nodo(coef, exp, actualNodo);
        else if (actualNodo.getNextNodo() == null && actualNodo.getExponente() > exp) {
            Nodo nodo = new Nodo(coef, exp, null);
            actualNodo.setNextNodo(nodo);
        } else {
            Nodo nodo = new Nodo(coef, exp, actualNodo);
            nuevoNodo.setNextNodo(nodo);
        }
        size++;
    }

    private void removeNodo(Nodo nuevoNodo, Nodo actualNodo) {
        if (nuevoNodo == actualNodo) this.raiz = actualNodo.getNextNodo();
        else nuevoNodo.setNextNodo(actualNodo.getNextNodo());

        actualNodo.setNextNodo(null);
        this.size--;
    }

}
